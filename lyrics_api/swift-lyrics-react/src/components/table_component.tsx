import React, { useEffect, useState } from 'react';
import { Table } from 'reactstrap';
import { LyricApiFp ,LyricApiFactory} from '../current_api/swift_api'



interface Song {
    id: number,
    name: string,
    album_name?: string,
    artist_name?: string
}

interface LyricRow{
    album: number,
    current_album: string,
    downvote: number,
    id: number,
    
    text:string,
    upvote: number,
    votes: number,
    song:Song
}
interface LyricArray{
    lyrics : LyricRow[]
}

export function TableLyricsBody({ lyrics } : LyricArray){
    

    const array_elements= lyrics.map( (item , index ,array) =>{

        return (
        <tr>
            <th scope="row">{index+1}</th>
            <td>{item.text}</td>
            <td>{item.song.name}</td>
            <td>{item.current_album}</td>
            <td>Edit</td>
            <td>Delete</td>
        </tr>
        )
    })

    return(
        <tbody>
            {array_elements}
        </tbody>
    )


}


export function TableLyrics (){

    const [rowNum, setRowNum] = useState(25);
    const [rowData, setRowData] = useState([])
    const [currentPage , setCurrentPage] = useState(1);
    const [nextPage , setNextPage] = useState(1)
    const [previousPage , setPreviousPage] = useState(0)
    const lyricFactory = LyricApiFactory()
    useEffect(() => {
       
        let resp =lyricFactory.lyricList(
            "",
            "",
            currentPage,
            rowNum
        ).then(
            resp=>{
                
                const next :string = resp.next

                if (next) {

                    setNextPage(currentPage+1)
                    
                }
                if (currentPage >1){
                    setPreviousPage(currentPage-1)
                }

                const results = resp.results;
                setRowData(results);
                
            }
        )
        

    },[rowNum , currentPage])


    return (
        <>
            <Table>
                <thead>
                    <tr>
                        <th></th>
                        <th>Lyrics</th>
                        <th>Song</th>
                        <th>Album</th>
                        <th></th>
                        <th></th>
                    </tr>
                </thead>
                
                <TableLyricsBody
                    lyrics={rowData}
                />

                
            </Table>
        </>
    );


}