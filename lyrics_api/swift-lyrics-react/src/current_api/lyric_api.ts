import { api_url } from '../constants/default_values'



export async function fetchLyricData(): Promise<any> {
    const base_url = `${api_url}/swiftlyrics/lyric/`
    let response = await fetch(base_url)
    return await response.json()
}