from drf_yasg import openapi
from rest_framework import serializers
from rest_framework.exceptions import APIException

from swift_lyrics.models import Artist, Lyric, Song, Album

import random

class BaseAlbumSerializer(serializers.ModelSerializer):

    artist_name = serializers.CharField(source="artist.name" , read_only=True )
    class Meta:
        model = Album
        fields = ['id', 'name' , 'year' , 'artist', "artist_name"]
    # album = BaseAlbumSerializer(source='song.album', read_only=True)

class BaseSongSerializer(serializers.ModelSerializer):
    album_name = serializers.CharField(source = "album.name" ,read_only=True)
    artist_name = serializers.CharField(source = "album.artist.name" , read_only=True)

    class Meta:
        model = Song
        fields = ['id', 'name' , "album_name" , "artist_name"]


class LyricSerializer(serializers.ModelSerializer):
    current_album = serializers.CharField(source="album.name" ,read_only=True ,allow_blank=True)
    downvote = serializers.IntegerField(read_only=True)
    upvote = serializers.IntegerField(read_only=True)
    votes= serializers.IntegerField(read_only=True)
# 'votes' , 'upvote' , 'downvote'
    class Meta:
        model = Lyric
        fields = ['id', 'text', 'votes' , 'upvote' , 'downvote' , "current_album"]
        extra_kwargs = {
            "votes" : {"read_only" :True},
            "downvote" : {"read_only" :True},
            "upvote" : {"read_only" :True}
        }


class AlbumDetailSerializer(BaseAlbumSerializer):
    songs = BaseSongSerializer(many=True, read_only=True)

    class Meta(BaseAlbumSerializer.Meta):
        fields = BaseAlbumSerializer.Meta.fields + ['songs']


class SongSerializer(BaseSongSerializer):
    album = BaseAlbumSerializer()

    class Meta(BaseSongSerializer.Meta):
        fields = BaseSongSerializer.Meta.fields + ['album']


class SongDetailSerializer(SongSerializer):
    lyrics = LyricSerializer(many=True, read_only=True)

    class Meta(SongSerializer.Meta):
        fields = SongSerializer.Meta.fields + ['lyrics']


class LyricDetailSerializer(LyricSerializer):
    song = BaseSongSerializer(read_only=True)
    

    def validate(self, data):
        

        print ( self.initial_data)
        song_id = self.initial_data.get('song', dict()).get('id', None)
        if song_id:
            # If song_id, then the album and song already exist, just fetch them from datastore
            song = Song.objects.get(id=song_id)
            data['song'] = song
        else:
            # If album_id, then album already exists - just fetch, then handle create/fetch song
            print ( self.initial_data)
            album_id = self.initial_data.get('album',None)# dict()).get('id', None)

            song = self.initial_data.get('song', dict())
            song_name = song.get('name', None)

            album = None
            if album_id:
                album = Album.objects.get(id=album_id)
            else:
                raise APIException(
                    detail={"msg"  :"Album Id not found"  } , code=400
                )
                # album_name = self.initial_data.get('album', dict()).get('name', None)
                # if album_name:
                #     album = Album.objects.filter(name=album_name).first()
                #     if album is None:
                #         album = Album(name=album_name)
                #         album.save()

            if song_name:
                song = Song.objects.filter(name=song_name).first()
                if song is None:
                    song = Song(name=song_name, album=album)
                    song.save()
                data['song'] = song

        return super().validate(data)

    def create(self, validated_data):
        lyric = Lyric(**validated_data)
        lyric.save()
        return lyric

    class Meta(LyricSerializer.Meta):
        fields = LyricSerializer.Meta.fields + ['song', 'album' ]
        extra_kwargs = {"album": {"required": True}}



class LyricVoteSerializer(serializers.ModelSerializer):
    class Meta:
        model = Lyric
        fields = ['id', 'upvote' , 'downvote']

    def validate(self, data):
        
        
        if data.get("upvote") and data.get('downvote'):
            raise APIException(
                detail={"msg":"You cannot send upvote/downvote at the same time"  },code=400
            )

        if data.get("upvote") == 0 and data.get("downvote") == 0 :

            raise APIException(
                detail={"msg":"You cannot send upvote/downvote with both at 0"  },code=400
            )

        if data.get("upvote"):
            if data.get("upvote") > 1 :
                raise APIException(
                    detail={"msg":"You cannot send an upvote number greater than 1"  },code=400
                )
        if data.get("downvote"):
            if data.get("downvote") > 1 :
                raise APIException(
                    detail={"msg":"You cannot send an downvote number greater than 1"  },code=400
                )
        return super().validate(data)
    
    def save(self ):
   
        validated_data:dict=self.validated_data
        instance:Lyric = self.instance

        if validated_data.get("upvote"):
            validated_data["upvote"] =instance.upvote + validated_data["upvote"]
        else:
            validated_data["upvote"] =instance.upvote 



        if validated_data.get("downvote"):
            validated_data["downvote"] =instance.downvote + validated_data["downvote"]
        else:
            validated_data["downvote"] =instance.downvote

        validated_data["votes"] = instance.votes+1


        return super().update( instance , validated_data )


class ArtistSerializer(serializers.ModelSerializer):
    
    class Meta:
        model = Artist
        fields = ["name" , "first_year" , "id"]

    def validate(self, data):
        return super().validate(data)




class CustomAlbumSerializer(serializers.ModelSerializer):

    
    class Meta:
        model = Album
        fields = ['id', 'name' ]

class ArtistDetailSerializer(serializers.ModelSerializer):
    albums =CustomAlbumSerializer(source = "album",read_only=True , many=True)
    class Meta:
        model = Artist
        fields = ["id" , "name" , "first_year" ,  "albums"]



class RandomLyricDetailSerializer(serializers.ModelSerializer):
    # song_name= serializers.CharField(source = "song.name",read_only=True)
    # artist_id = serializers.IntegerField(source = "song.album.artist.id")
    class Meta:
        model = Lyric
        fields = ["id" , "text"]
        # extra_kwargs={"artist_id" : {"required":False} }