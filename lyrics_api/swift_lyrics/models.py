from django.db import models


class Artist(models.Model):
    name = models.TextField(
        null=False , 
        blank=False ,
        help_text="Artist name - Artist name name required",
        unique=True
        )
    first_year = models.IntegerField(
        null=True,
    )


class Album(models.Model):
    name = models.TextField(
        blank=False,
        db_index=True,
        unique=True,
        help_text="Album name - can alternatively use 'id' field set to id of existing album when creating new lyrics")
    year = models.IntegerField(
        null=True
    )
    artist = models.ForeignKey(
        Artist,
        null=True,
        on_delete=models.CASCADE,
        related_name="album"
    )

    class Meta:

        unique_together = ('artist_id', 'name',)



    objects = models.Manager()


class Song(models.Model):
    name = models.TextField(
        blank=False,
        db_index=True,
        unique=True,
        help_text="Song name - can alternatively use 'id' field set to id of existing song when creating new lyrics")

    album = models.ForeignKey(
        Album,
        related_name='songs',
        null=True,
        on_delete=models.CASCADE,
        help_text="Album")

    objects = models.Manager()


class Lyric(models.Model):
    text = models.TextField(
        blank=False,
        db_index=True,
        help_text="Lyrics from a song/album")

    song = models.ForeignKey(
        Song,
        related_name='lyrics',
        null=True,
        on_delete=models.CASCADE,
        help_text="Song")

    votes = models.IntegerField(
        default=0
    )

    upvote= models.IntegerField(
        default=0,
        null=False,
        help_text="Number of upvotes for the current lyric"
    )
    downvote= models.IntegerField(
        default=0,
        null=False,
        help_text="Number of downvotes for the current lyric"
    )
    album = models.ForeignKey(
        Album,
        null=True,
        on_delete=models.CASCADE
    )

    objects = models.Manager()
