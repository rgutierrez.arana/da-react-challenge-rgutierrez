import random
from typing import List
from django.http.response import JsonResponse
from drf_yasg import openapi
from rest_framework import mixins, generics, filters, serializers, status
from rest_framework.response import Response

from django.http import HttpResponse
from django.views import View
from rest_framework.views import APIView

# Create your views here.
from swift_lyrics.models import Lyric, Album, Song , Artist
from rest_framework.exceptions import APIException
from drf_yasg.utils import swagger_auto_schema

from swift_lyrics.common.swift_filters import artist_filter , random_detail_response,artist_year_filter_lt , artist_year_filter_gt

from swift_lyrics.serializers.serializer import (ArtistDetailSerializer, LyricSerializer,
                                                 BaseSongSerializer,
                                                 BaseAlbumSerializer,
                                                 AlbumDetailSerializer,
                                                 SongDetailSerializer,
                                                 LyricSerializer,
                                                 SongSerializer,
                                                 LyricDetailSerializer,
                                                 LyricVoteSerializer,
                                                 ArtistSerializer,
                                                 RandomLyricDetailSerializer)


class HealthCheckView(View):
    """
    Checks to see if the site is healthy.
    """
    def get(self, request, *args, **kwargs):
        return HttpResponse("ok")


class AlbumIndex(mixins.ListModelMixin,

                mixins.CreateModelMixin,
                 generics.GenericAPIView):
    serializer_class = BaseAlbumSerializer

    def get_queryset(self):
        return Album.objects.all()

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        return self.create(request, *args, **kwargs)


class AlbumDetail(mixins.RetrieveModelMixin,
                  mixins.DestroyModelMixin,
                generics.GenericAPIView):
    serializer_class = AlbumDetailSerializer

    def get_queryset(self):
        return Album.objects.all()

    def get(self, request, *args, **kwargs):
        return self.retrieve(request, *args, **kwargs)

    def delete(self, request, *args, **kwargs):
        return self.destroy(request, *args, **kwargs)


class SongIndex(mixins.ListModelMixin,
                 generics.GenericAPIView):
    serializer_class = SongSerializer

    def get_queryset(self):
        return Song.objects.all()

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)


class SongDetail(mixins.RetrieveModelMixin,
                 mixins.DestroyModelMixin,
                generics.GenericAPIView):
    serializer_class = SongDetailSerializer

    def get_queryset(self):
        return Song.objects.all()

    def get(self, request, *args, **kwargs):
        return self.retrieve(request, *args, **kwargs)

    def delete(self, request, *args, **kwargs):
        return self.destroy(request, *args, **kwargs)


class APIIndex(mixins.ListModelMixin,
               mixins.CreateModelMixin,
               generics.GenericAPIView):
    serializer_class = LyricDetailSerializer
    filter_backends = [filters.SearchFilter, filters.OrderingFilter]
    search_fields = ['text', 'song__name', 'song__album__name']
    ordering_fields = ['text', 'song__name', 'song__album__name']


    def get_queryset(self):
        return Lyric.objects.all()

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        return self.create(request, *args, **kwargs)


class APIDetail(mixins.RetrieveModelMixin,
                mixins.UpdateModelMixin,
                mixins.DestroyModelMixin,
                generics.GenericAPIView):
    serializer_class = LyricDetailSerializer

    def get_queryset(self):
        return Lyric.objects.all()

    def get(self, request, *args, **kwargs):
        return self.retrieve(request, *args, **kwargs)

    def delete(self, request, *args, **kwargs):
        return self.destroy(request, *args, **kwargs)

class APIVoteDetail(
                mixins.UpdateModelMixin,
                generics.GenericAPIView):
    serializer_class = LyricVoteSerializer
    
    def get_serializer(self, *args, **kwargs):
        return super().get_serializer(*args, **kwargs)
    def get_queryset(self):
        return Lyric.objects.all()



    def put(self, request, *args, **kwargs):

        
        return self.update(request , *args , **kwargs)





class ArtistIndex(
    mixins.ListModelMixin,
    mixins.CreateModelMixin,
    generics.GenericAPIView
):

    serializer_class = ArtistSerializer
    filter_backends = [ filters.OrderingFilter]

    
    ordering_fields = [ 'first_year', 'name']

    def filter_queryset(self, queryset):
        for backend in list(self.filter_backends):
            queryset = backend().filter_queryset(self.request, queryset, self)
        return queryset

    def fiter_by_year_interval(self) -> List[Artist]:
        
        #*Function that takes 2 intervals from the query params in order to filter them and return the base qs
        

        artist_year_filter_gt:int = self.request.query_params.get("artist_year_filter_gt" , None)

        artist_year_filter_lt:int = self.request.query_params.get("artist_year_filter_lt" , None)

        base_qs  =Artist.objects

        query_param = {}

        if artist_year_filter_gt:
            query_param["first_year__gt"] = artist_year_filter_gt
        if artist_year_filter_lt:
            query_param["first_year__lt"] = artist_year_filter_lt


        base_qs=base_qs.filter(**query_param)

      
        return base_qs
        
    def get_queryset(self):
        #?We apply the filters to the queryset    
        current_qs = self.fiter_by_year_interval()
        return current_qs.all()

    #*We add a custom schema in order to filter data
    @swagger_auto_schema(manual_parameters=[artist_year_filter_gt , artist_year_filter_lt])
    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        return self.create(request, *args, **kwargs)
    

class ArtistDetail(
    mixins.RetrieveModelMixin,
    mixins.UpdateModelMixin,
    mixins.DestroyModelMixin,
    generics.GenericAPIView,
):

    serializer_class = ArtistDetailSerializer

    def get_queryset(self):
        
        return Artist.objects.all()

    def get(self, request, *args, **kwargs):
        return self.retrieve(request, *args, **kwargs)
    def put(self, request, *args, **kwargs):
      
        return self.update(request , *args , **kwargs)

    def delete(self, request, *args, **kwargs):
        return self.destroy(request, *args, **kwargs)






class APIRandomDetail(APIView):

    @swagger_auto_schema(manual_parameters=[artist_filter] , responses={200 : random_detail_response})
    def get(self,request):
        artist_filter = request.query_params.get("artist_filter" , None)

        lyrics_arr:List[Lyric]=Lyric.objects
        if artist_filter:
            lyrics_arr=lyrics_arr.filter(album__artist__id = artist_filter).all()
        else:
            lyrics_arr=lyrics_arr.all()
        
        if len(lyrics_arr ) == 0 :
            raise APIException(detail={"msg" : "The artist that you selected doesn't have any song assigned"} , code=400)
        selected_lyric = random.choice(lyrics_arr)

        
        return JsonResponse(data={
            "id" : selected_lyric.pk,
            "text" : selected_lyric.text 
        } )
