from drf_yasg import openapi


from swift_lyrics.serializers.serializer import RandomLyricDetailSerializer

artist_filter = openapi.Parameter("artist_filter",
                                  openapi.IN_QUERY,
                                  description="id artist filter",
                                  type=openapi.TYPE_INTEGER)


random_detail_response = openapi.Response("random item" , RandomLyricDetailSerializer)



artist_year_filter_gt =openapi.Parameter(
    "artist_year_filter_gt",
    openapi.IN_QUERY,
    description="year of operation greater than this filter",
    type=openapi.TYPE_INTEGER
)


artist_year_filter_lt =openapi.Parameter(
    "artist_year_filter_lt",
    openapi.IN_QUERY,
    description="year of operation lower than this filter",
    type=openapi.TYPE_INTEGER
)

