# Create your tests here.
from django.test import TestCase , Client

from swift_lyrics.models import Artist, Lyric, Song, Album
from urllib.parse import urlencode

content_type = "application/json"


def add_urrlencode (url:str , params:dict):

  encoded_params = urlencode(params)
  return f"{url}?{encoded_params}"

class LyricTestCase(TestCase):
    
    client :Client= Client()
    current_lyric:int=None
    def __init__(self, methodName: str) -> None:

        super().__init__(methodName=methodName)


    def post_client (self,url:str ,data:dict , content_type:str =content_type):
      response = self.client.post(path = url , data=data , content_type=content_type)
      return response

    def put_client (self,url:str ,data:dict , content_type:str =content_type):
      response = self.client.put(path = url , data=data , content_type=content_type)
      return response

    def get_client (self, url:str , data:dict=None ):
      url_encoded = url
      if data :
        url_encoded=add_urrlencode(url = url , params=data)


      response = self.client.get(path=url_encoded)

      return response
      
    def delete_client (self,url:str ):
      response = self.client.delete(path = url )
      return response



    def test_aupvote_downvote_lyric(self):
        #! Lyrics can be upvoted/downvoted by lyric ID

        lyric_data = {
          "text": "nueva cancioan",
          "song": {
            "name": "cancion 0 "
          },
          "album": 1
        }
        
        response = self.post_client(url = "/swiftlyrics/lyric/" , data=lyric_data )

        assert response.status_code ==201 , "Lyric creation fail"
        resp = response.json()

        print(resp)

        resp_lyric_vote = self.put_client(url=f'/swiftlyrics/vote/lyric/{resp["id"]}/', data={"upvote" : 1 } )

        assert resp_lyric_vote.status_code == 200 , "Lyric vote update fail"

        resp_lyric_vote = self.put_client(url=f'/swiftlyrics/vote/lyric/{resp["id"]}/', data={"downvote" : 1 } )
        assert resp_lyric_vote.status_code == 200 , "Lyric vote update fail"


        current_lyric :Lyric= Lyric.objects.filter(pk = resp["id"]).first()

        assert current_lyric.upvote == 1, f"The upvote of the current lyric [id ={resp['id']} ] have failed "
        assert current_lyric.downvote == 1, f"The downvote of the current lyric [id ={resp['id']} ] have failed "

    def test_counted_lyrics_serializers(self):

      
      #! Include upvote/downvote count on the lyrics serializers
      
      base_url = "/swiftlyrics/lyric/"

      query_params = {
        "page" : 1,
        "size" : 10
      }
      encoded_url = add_urrlencode(url=base_url, params=query_params)

      response = self.client.get(path=encoded_url)

      assert response.status_code == 200, "Cannot access the get lyric list endpoint"

      resp = response.json()

      results = resp["results"]

      for result in results:

        assert type ( result["upvote"]) ==int , f"Upvote doesn't exist for {result}"
        assert type ( result["downvote"]) ==int , f"Upvote doesn't exist for {result}"
        assert type ( result["votes"]) ==int , f"Upvote doesn't exist for {result}"
      
      current_lyric = result["id"]


      #! Testing the detail parameter
      url = f"/swiftlyrics/lyric/{current_lyric}/"
      response = self.client.get(path=url)
      assert response.status_code == 200, "Cannot access the get lyric endpoint"
      result = response.json()




      assert type ( result["upvote"]) ==int , f"Upvote doesn't exist for {result}"
      assert type ( result["downvote"]) ==int , f"Upvote doesn't exist for {result}"
      assert type ( result["votes"]) ==int , f"Upvote doesn't exist for {result}"


    def test_make_album_creation(self):

      #!Make album creation separate from Lyric creation
      
      #!Add year to album model (year is required on create, but existing albums can be null)
      #!Name must be unique to artist
      #!Year, name and artist all required - return appropriate HTTP error code when not included
      


      data = {
        "name": "awsome album",
        "year": 2020,
        "artist": 1
      }
      url = "/swiftlyrics/album/"

      response = self.client.post(path =url, data=data , content_type=content_type)
      assert response.status_code ==201 , f"Album creation fail - {response.content}"

      album_create_resp = response.json()

      response = self.client.post(path =url, data=data , content_type=content_type)


      assert response.status_code ==400 , f"Album duplication  - {response.content}"
      


      #!Change lyric creation to require ID of an already existing album
      lyric_data = {
        "text": "NEW YEAR SONG",
        "song": {
          "name": "xmass song "
        },
        "album": album_create_resp["id"]
      }
      
      response = self.client.post(path = "/swiftlyrics/lyric/" , data=lyric_data , content_type=content_type)

      assert response.status_code ==201 , "Lyric creation fail"
    

    def test_artist_support(self):
      #!Create, Delete, List, Update, and Get by ID
      data= {
        "name": "Shakira",
        "first_year": 1970
      }
      url  ="/swiftlyrics/artist/"
      response = self.post_client(url =url, data=data)

      assert response.status_code == 201, f"Cannot create new artist {data}"


      created_artist = response.json()

      #!List 
      url = "/swiftlyrics/artist/"
      query_params = {
        "page" : 1,
        "size" : 10
      }
      response = self.get_client(url=url, data=query_params)

      assert response.status_code == 200, "Cannot access the get artist list endpoint"

      resp = response.json()

      results = resp["results"]

      assert type(results) == list , "Error the artist list response is not a list"


      #!Get by ID
      #/swiftlyrics/artist/1/
      url=f'/swiftlyrics/artist/{created_artist["id"]}/'

      response = self.get_client(url=url)

      assert response.status_code == 200, f"Cannot access the get artist detail endpoint - {response.content}"

      artist_response = response.json()

      assert type (artist_response.get("albums") ) == list , "The artist structure doesns't have albums"


      #!Update,
      data= {
        "name": "Shakira_2 electric bogaloo",
        "first_year": 1990
      }

      response = self.put_client(url=url , data= data)
      assert response.status_code == 200, f"Cannot update the artist - {response.content}"

      current_artist:Artist  = Artist.objects.filter(pk=created_artist["id"]).first()

      assert current_artist.name == data["name"] , f"NAME: Not update the name of the artist {data['name']}"

      assert current_artist.first_year == data["first_year"] , f"FIRST YEAR: Not update the first_year of the artist {data['name']}"


      #!Delete

      response = self.delete_client(url=url)
      assert response.status_code == 204, f"Cannot delete the artist - {response.content}"


      current_artist:Artist  = Artist.objects.filter(pk=created_artist["id"]).first()

      assert not(current_artist ), f"The artist was not deleted"



    def test_random_lyric(self):
      url = "/swiftlyrics/artist/lyric/random/"
      data = {
        "artist_filter" : ""
      }
      result = self.get_client(url=url , data=data)

      assert result.status_code == 200, f"Cannot access the Random Lyric endpoint - {result.content}"


      resp = result.json()

      print ( resp)


